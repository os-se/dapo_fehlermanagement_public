package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import de.hfu.unit.Queue;
import de.hfu.unit.Util;
import de.hfu.integration.*;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Beispiele für Unit-Tests
 */
@DisplayName("Test der Klasse App")
public class AppTest
{
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Test
    void mainTest() {
        App app = new App();

        String terminalInput = "Test";
        ByteArrayInputStream testIn = new ByteArrayInputStream(terminalInput.getBytes());
        System.setIn(testIn);

        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        app.main(null);

        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        String output = outContent.toString();

        assertEquals("Eingabe: Ausgabe: HALLO WELT\n", output);

        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    /**
     * Tests fuer erstes-Halbjahr-Pruefung
     */

    @Test
    void istErstesHalbjahrTest(){
        Util util = new Util();
        for(int i=1; i<=12; i++)
            if(i<7)
                assertTrue(Util.istErstesHalbjahr(i));
            else
                assertFalse(Util.istErstesHalbjahr(i));

        assertThrows(IllegalArgumentException.class, () -> {
            Util.istErstesHalbjahr(13);
        }, "IllegalArgumentException was expected");

        assertThrows(IllegalArgumentException.class, () -> {
            Util.istErstesHalbjahr(-1);
        }, "IllegalArgumentException was expected");
    }

    /**
     * Tests fuer Queue Klasse
     */
    @Test
    void QueueTest(){
        Queue queue = new Queue(3);
        assertThrows(IllegalStateException.class, () -> {
            queue.dequeue();
        }, "IllegalStateException was expected");
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);   // override a=3
        queue.enqueue(5);   // override a=4
        assertTrue(queue.dequeue() == 1);
        assertTrue(queue.dequeue() == 2);
        assertTrue(queue.dequeue() == 5);
        assertThrows(IllegalStateException.class, () -> {
            queue.dequeue();
        }, "IllegalStateException was expected");

        assertThrows(IllegalStateException.class, () -> {
            queue.dequeue();
        }, "IllegalStateException was expected");

        assertThrows(IllegalArgumentException.class, () -> {
            Queue newQueue = new Queue(0);
        }, "IllegalArgumentException was expected");
    }

    /**
     * Wird einmal vor allen Tests dieser Klasse aufgerufen
     */
    @BeforeAll
    static void initAll() {
    }

    /**
     * Wird jeweils vor den Tests dieser Klasse aufgerufen
     */
    @BeforeEach
    void init() {
    }

    /**
     * Wird immer erfolgreich sein
     */
    @Test
    void succeedingTest() {
        assertEquals(1, 1, "ist immer gleich");
    }

    /**
     * Wird niemals erfolgreich sein
     * Deshalb besser deaktiviert
     */
    @Test
    @Disabled("kein sinnvoller Test, deshalb deaktiviert")
    void failingTest() {
        fail("dieser Test schlägt fehl");
    }

    /**
     * Wird jeweils nach den Tests dieser Klasse aufgerufen
     */
    @AfterEach
    void tearDown() {
    }

    /**
     * Wird einmal nach allen Tests dieser Klasse aufgerufen
     */
    @AfterAll
    static void tearDownAll() {
    }

}