package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.easymock.EasyMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ResidentTest {
    //::::::::::::::::::::://
    //::                 :://
    //::   Stub-Objekt   :://
    //::                 :://
    //::::::::::::::::::::://

    @Test
    void residentTestStub(){
        BaseResidentService baseResidentService = new BaseResidentService();
        baseResidentService.setResidentRepository(new ResidentRepositoryStub());
        ResidentRepositoryStub rpStub = new ResidentRepositoryStub();
        List<Resident> allResidents = rpStub.getResidents();

        // test-Methode
        residentTest(allResidents, baseResidentService);
    }

    //::::::::::::::::::://
    //::               :://
    //::   Easy-Mock   :://
    //::               :://
    //::::::::::::::::::://

    @Test
    void residentMockTest(){
        BaseResidentService baseResidentService = new BaseResidentService();
        List<Resident> residentList = new ArrayList<>();

        residentList.add(new Resident("David", "Pospisil1", "Bahnhofstrasse1", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("David", "Pospisil1", "Bahnhofstrasse1", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Jonas", "Pospisil2", "Bahnhofstrasse2", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Sarah", "Pospisil3", "Bahnhofstrasse3", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Daniel", "Pospisil4", "Bahnhofstrasse4", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Johannes", "Pospisil5", "Bahnhofstrasse5", "Furtwangen", getDate("2004-07-29")));
        residentList.add(new Resident("Fabian", "Pospisil6", "Bahnhofstrasse6", residentList.get(0).getCity(), getDate("2004-07-29")));

        ResidentRepository residentRepositoryMock = mock(ResidentRepository.class);
        expect(residentRepositoryMock.getResidents()).andReturn(residentList).anyTimes();

        replay(residentRepositoryMock);

        baseResidentService.setResidentRepository(residentRepositoryMock);

        verify(residentRepositoryMock);

        // test-Methode
        residentTest(residentRepositoryMock.getResidents(), baseResidentService);
    }

    //::::::::::::::::::::::://
    //::                   :://
    //::   resident Test   :://
    //::                   :://
    //::::::::::::::::::::::://

    void residentTest(List<Resident> allResidents, BaseResidentService service){

        //******************************//
        //  getFilteredResidentsList()  //
        //******************************//

        // test case #1 | all Residents
        Resident filteredResident = new Resident();
        filteredResident = new Resident("*", "*", "*", "*", getDate("2004-07-29"));
        List<Resident> correctResidentList = allResidents;

        for(int i=0; i<correctResidentList.size(); i++)
            assertEquals(correctResidentList.get(i), (service.getFilteredResidentsList(filteredResident).get(i)));

        // test case #2 | all names with D*
        filteredResident = new Resident("D*","*o*", "B*", "Furtwange*", getDate("2004-07-29"));
        correctResidentList = new ArrayList<>();
        correctResidentList.add(allResidents.get(0));
        correctResidentList.add(allResidents.get(1));
        correctResidentList.add(allResidents.get(4));

        for(int i=0; i<correctResidentList.size(); i++)
            assertEquals(correctResidentList.get(i), (service.getFilteredResidentsList(filteredResident).get(i)));

        // test case #3 | one specific name
        filteredResident = allResidents.get(3);
        correctResidentList = new ArrayList<>();
        correctResidentList.add(allResidents.get(3));
        assertEquals(correctResidentList.get(0), (service.getFilteredResidentsList(filteredResident).get(0)));


        //***********************//
        //  getUniqueResident()  //
        //***********************//

        // test case #4 | more than one returned value
        filteredResident = allResidents.get(0);
        try{
            service.getUniqueResident(filteredResident);
            assert(false);
        } catch (ResidentServiceException ex) {
            assert(true);
        }

        // test case #5 | correct returned value
        filteredResident = allResidents.get(2);
        Resident correctResident = allResidents.get(2);
        try{
            assertEquals(correctResident, service.getUniqueResident(filteredResident));
        } catch (ResidentServiceException ex) {
            assert(false);
        }

        // test case #6 | Resident invalid
        filteredResident = new Resident(allResidents.get(2));
        filteredResident.setGivenName("JonasNeu");
        filteredResident.setFamilyName("Hauser");
        filteredResident.setStreet("Litschlestraße 10");
        filteredResident.setDateOfBirth(getDate("2006-06-02"));
        filteredResident.setCity("Frankfurt");
        try{
            System.out.println(service.getUniqueResident(filteredResident));
            assert(false);
        } catch (ResidentServiceException ex) {
            assert(true);
        }

        // test case #7 | invalid wildcard
        filteredResident = new Resident(allResidents.get(0));
        filteredResident.setGivenName("D*");
        try{
            service.getUniqueResident(filteredResident);
            assert(false);
        } catch (ResidentServiceException ex) {
            assert(true);
        }

        assertFalse(filteredResident.equals(null));
    }

    Date getDate(String strDate){
        Date date = null;
        String filterDate = strDate;
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try{
            date = dateFormatter.parse(filterDate);
        } catch (ParseException ex){ System.out.println("Error"); }
        return date;
    }
}
