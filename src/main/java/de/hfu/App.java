package de.hfu;

import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {

        // :::::::::::::::::::::::::::::::://
        //::                             :://
        //::  Neue Änderung für Jenkins  :://
        //::                             :://
        // :::::::::::::::::::::::::::::::://


        System.out.print("Eingabe: ");
        Scanner sInput = new Scanner("Hallo Welt");

        System.out.println("Ausgabe: " + sInput.nextLine().toUpperCase());
    }
}